<!DOCTYPE html>
<html>
<head>
	<title>
	<?php get_title(); ?></title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/flatly/bootstrap.css">
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
	  <a class="navbar-brand" href="catalog.php">FR CORDOVAN</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarColor03">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item active">
	        <a class="nav-link" href="catalog.php">Home <span class="sr-only">(current)</span></a>
	      </li>

	      <?php 
	      	session_start();
	      	if (isset($_SESSION['user']) && $_SESSION['user']['role_id']==1) {
	      	?>

	      	

	      <li class="nav-item">
	        <a class="nav-link" href="add-item.php">Add Item</a>
	      </li>

	      <?php
	  
	      	}else {
	      	?>

	      <li class="nav-item">
	        <a class="nav-link" href="cart.php">Cart<span class="badge bg-warning" id="cartCount">
	        	<?php 
	        		if(isset($_SESSION['cart'])){
	        			echo arraY_sum($_SESSION['cart']);
	        		}else{
	        			echo 0;
	        		}
	        	 ?>
	        	</span>
	    	 </a>
	      </li>
	      <?php
	      	}
	      	if(isset($_SESSION['user'])){
	      	?>
	      	<li class="nav-item">
	      		<a href="profile.php" class="nav-link">Hello 
	      			<?php echo $_SESSION['user']['firstName'] ?>!

	      		</a>
	      	</li>
	      	<li class="nav-item">
	      		<a href="purchase-history.php" class="nav-link">Purchase History</a>
	      	</li>
	      	<li class="nav-item">
	      		<a href="../controllers/logout-process.php" class="nav-link">Logout</a>
	      	</li>

	      	<?php
	      	}else{
	      	?>

		      <li class="nav-item">
		        <a class="nav-link" href="register.php">Register</a>
		      </li>
		       <li class="nav-item">
		        <a class="nav-link" href="login.php">Login</a>
		      </li>
	      <?php
	      	}
	       ?>

	    </ul>
	    <form class="form-inline my-2 my-lg-0">
	      <input class="form-control mr-sm-2" type="text" placeholder="Search">
	      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
	    </form>
	  </div>
	</nav>


	<?php get_body_contents() ?>

		<!-- Footer -->
		<footer class="page-footer font-small bg-light mt-5">
			<div class="footer-copyright text-center py-3">Terms & Conditions | Privacy Policy
				Copyright 2020 © | FR CORDOVAN <br> by Fernae Rillera</div>
		</footer>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>