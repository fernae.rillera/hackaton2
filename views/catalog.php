<?php 
	require "../partials/template.php";
	function get_title(){
		echo "Catalog";

		}

	function get_body_contents(){
	// require connection
	require "../controllers/connection.php";

	?>

<div class="container mt-5"> 
	<h1 class="text-center py-5">Items Menu</h1>


	<!-- item lists -->
		<div class="row d-flex align-items-center justify-content-center">
		<?php 
		// publish items from sql database
		$items_query = "SELECT * FROM items";
		$items = mysqli_query($conn, $items_query);

		foreach ($items as $indiv_item) {
		?>


		<div class="lg-4 py-2">
			<div class="card m-3">
				<img src="<?php echo $indiv_item['imgPath'] ?>" class="card-img-top" height="400px" style="object-fit: cover; width: 300px;">
				<div class="card-body">
					<h4 class="card-title"><?= $indiv_item['name']; ?></h4>
				</div>
				<div class="card-body text-center">
					<p class="card-text">Price: Php <?= $indiv_item['price']?></p>
					<p class="card-text">Description: <?= $indiv_item['description']?></p>
					<p class="card-text">Category: 
						<?php 
							$catId = $indiv_item['category_id'];
							$category_query = "SELECT * FROM categories WHERE id = $catId";
							$category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));

							echo $category['name'];
						 ?>
					</p>
				</div>


					<?php 
							if(isset($_SESSION['user']['role_id']) && $_SESSION['user']['role_id']== 1){
						?>
				<div class="card-footer text-center">
					<a href="../controllers/delete-item-process.php?id=<?= $indiv_item['id']?>" class="btn btn-danger">Delete Item</a>
					<a href="edit-item.php?id=<?=$indiv_item['id']?>" class="btn btn-success">Edit Item</a>
				</div>
			<?php 
		}
		 ?>


				<div class="card-footer text-center">
					<input 
						type="number" 
						name="cart" 
						class="form-control" 
						value="1"
					>
					<button 
						class="btn btn-info mt-2 addToCart" 
						data-id="<?php echo $indiv_item['id']; ?>" 
						type="button">Add to Cart
					</button>	
						<!-- </form> -->
				</div>	
			</div>
		</div>

		<?php
			}
		 ?>
		</div>
	</div>
	<script type="text/javascript" src="../assets/scripts/add-to-cart.js"></script>
<?php
	}	
 ?>