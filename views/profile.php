<?php 


	require "../partials/template.php";
	function get_title(){
		echo "Profile";
	}
	function get_body_contents(){
			require "../controllers/connection.php";
			// session_destroy();

			$firstName = $_SESSION['user']['firstName'];
			$lastName = $_SESSION['user']['lastName'];
			$email = $_SESSION['user']['email'];
			$profilePic = "";
 ?>






<div class="container mt-5"> 
	<h1 class="text-center py-5">Profile Page</h1>

<div class="container">
<div class="row">
	<div class="col-lg-6">
		<img src="../assets/images/<?php echo $profilePic ?>" style="
		height: 150px; 
		width: 150px; 
		border: 1px solid skyblue;" >
		<form action="../controllers/add-profile-image-process.php" method="POST" enctype="multipart/form-data">
		<div class="form-group">
				<input type="file" name="image" class="form-control w-50" >
		</div>
		</form>
		


		<br>
		<h4 >
			first name: <span class="text-success"><?php echo $firstName ?></span>
		</h4>
		<h4 >
			last name: <span class="text-success"><?php echo $lastName ?></span>
		</h4>
		<h4>
				email: <span class="text-success"><?php echo $email ?></span>
		</h4>	

			<h3>Addresses: </h3>
			<ul>
					<?php 
					$userId= $_SESSION['user']['id'];
					$address_query = "SELECT * FROM addresses WHERE user_id = $userId";
					$addresses = mysqli_query($conn, $address_query);
					foreach($addresses as $indiv_address){
					?>

					<li>
						<?php echo $indiv_address['address1']. ", ". $indiv_address['address2']. "<br>". $indiv_address['city']. " ". $indiv_address['zipCode'] ?>
					</li>

				 <?php 
					}
				?>

			</ul>




			<h3>Contact Numbers: </h3>
			<ul>
				<?php 
				$contacts_query = "SELECT * FROM contacts WHERE user_id = $userId";
				$contacts = mysqli_query($conn, $contacts_query);
				foreach($contacts as $indiv_contacts){
					?>
					<li>
						<?php echo $indiv_contacts['contactNo'];	?>
					</li>

					<?php
				}
				 ?>

			</ul>



	</div>
	<div class="col-lg-6">

				<form action="../controllers/add-address-process.php" method="POST">
					<div class="form-group">
						<label for="address1">
							Address 1:
						</label>
						<input type="text" name="address1" class="form-control">
					</div>
					<div class="form-group">
						<label for="address2">
							Address 2:
						</label>
						<input type="text" name="address2" class="form-control">
					</div>
					<div class="form-group">
						<label for="City">
							City: 
						</label>
						<input type="text" name="city" class="form-control">
					</div>
					<div class="form-group">
						<label for="zipCode">
							Zip Code:
						</label>
						<input type="number" name="zipCode" class="form-control">
					</div>
					<input type="hidden" name="user_id" value="<?php echo $userId ?>">
					<button class="btn btn-info" type="submit">Add Address</button>
				</form>

				<br>





				<form action="../controllers/add-contacts-process.php" method="POST">
					<div class="form-group">
						<label for="contacts">
							Contact Number:
						</label>
						<input type="number" name="contacts" class="form-control">
					</div>
					<input type="hidden" name="user_id" value="<?php echo $userId ?>">
					<button class="btn btn-info" type="submit">Add Number</button>
				</form>






	</div>
	</div>

 </div>



























 <?php 
 }
  ?>