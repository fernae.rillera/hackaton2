<?php 
	
	require "connection.php";



	// image handling
	$image = $_FILES['image'];
	$file_types = ["jpg", "jpeg", "png", "gif", "tiff", "tif"];
	$file_ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));


	$destination = "../assets/images/";
	$fileName = $image['name'];
	$imgPath = $destination.$fileName;
	move_uploaded_file($image['tmp_name'], $imgPath);

	header("Location: ".$_SERVER['HTTP_REFERER']);


?>